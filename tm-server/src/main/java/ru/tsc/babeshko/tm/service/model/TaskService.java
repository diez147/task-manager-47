package ru.tsc.babeshko.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.babeshko.tm.api.repository.model.ITaskRepository;
import ru.tsc.babeshko.tm.api.repository.model.IUserRepository;
import ru.tsc.babeshko.tm.api.service.IConnectionService;
import ru.tsc.babeshko.tm.api.service.model.ITaskService;
import ru.tsc.babeshko.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.babeshko.tm.exception.entity.UserNotFoundException;
import ru.tsc.babeshko.tm.exception.field.EmptyDescriptionException;
import ru.tsc.babeshko.tm.exception.field.EmptyNameException;
import ru.tsc.babeshko.tm.exception.field.EmptyUserIdException;
import ru.tsc.babeshko.tm.model.Task;
import ru.tsc.babeshko.tm.model.User;
import ru.tsc.babeshko.tm.repository.model.TaskRepository;
import ru.tsc.babeshko.tm.repository.model.UserRepository;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;

public class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    protected ITaskRepository getRepository(@NotNull final EntityManager entityManager) {
        return new TaskRepository(entityManager);
    }

    @NotNull
    protected IUserRepository getUserRepository(@NotNull final EntityManager entityManager) {
        return new UserRepository(entityManager);
    }

    @Nullable
    @Override
    public Task create(@Nullable final User user, @Nullable final String name) {
        if (user == null) throw new UserNotFoundException();
        if (user.getId().isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable Task task = new Task();
        task.setName(name);
        task.setUser(user);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Nullable
    @Override
    public Task create(
            @Nullable final User user,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (user == null) throw new UserNotFoundException();
        if (user.getId().isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUser(user);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Nullable
    @Override
    public Task create(
            @Nullable final User user,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        if (user == null) throw new UserNotFoundException();
        if (user.getId().isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUser(user);
        task.setDateBegin(dateBegin);
        task.setDateEnd(dateEnd);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepository repository = getRepository(entityManager);
            return repository.findAllByProjectId(userId, projectId);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAllByProjectId(final @Nullable String userId, final @Nullable String projectId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectNotFoundException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeAllByProjectId(userId, projectId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeAllByUserId(final @Nullable String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ITaskRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeAllByUserId(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}