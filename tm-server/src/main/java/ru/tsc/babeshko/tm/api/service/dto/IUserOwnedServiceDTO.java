package ru.tsc.babeshko.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.tsc.babeshko.tm.api.repository.dto.IUserOwnedRepositoryDTO;
import ru.tsc.babeshko.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.tsc.babeshko.tm.enumerated.Status;

import java.util.Date;

public interface IUserOwnedServiceDTO<M extends AbstractUserOwnedModelDTO> extends IUserOwnedRepositoryDTO<M>, IServiceDTO<M> {

    @Nullable
    M create(@Nullable String userId, @Nullable String name);

    @Nullable
    M create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @Nullable
    M create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description,
            @Nullable Date dateBegin,
            @Nullable Date dateEnd
    );

    @Nullable
    M updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @Nullable
    M changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

}